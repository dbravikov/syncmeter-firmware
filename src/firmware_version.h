#pragma once
#ifndef VERSION_H_20170329_231901
#define VERSION_H_20170329_231901


/* Возвращает версию прошивки ввиде строки. Версия задается в файле main.c. */
const char * firmwareVersion(void);


#endif /* #ifndef VERSION_H_20170329_231901 */
