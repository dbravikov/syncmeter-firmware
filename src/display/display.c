#include "display.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

static bool displayReady = false;

bool initializeDisplay(void);

int print(const char * format, ...) {
	if (displayReady == false) {
		if (initializeDisplay()) {
			displayReady = true;
		}
		else {
			return -1;
		}
	}

    char * strp;

    va_list args;
    va_start(args, format);
    const int stringLength = vasprintf(&strp, format, args);
    va_end(args);

    if (stringLength < 0) {
        return stringLength;
    }

    // ...

    free(strp);

	return -1;
}

bool initializeDisplay(void) {
	return false;
}
