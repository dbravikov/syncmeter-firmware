#include "stm32f0xx_it.h"
#include "stm32f0xx.h"


/******************************************************************************/
/*            Cortex-M0 Processor Exceptions Handlers                         */
/******************************************************************************/


void NMI_Handler(void) {
}


void HardFault_Handler(void)
{
	while (1);
}


void SVC_Handler(void)
{
}


void PendSV_Handler(void)
{
}


void SysTick_Handler(void)
{
	HAL_IncTick();
}
